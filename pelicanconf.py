#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Baptiste BEAUPLAT <lyknode@cilg.org>'
SITENAME = 'Debian Nantes'
SITEURL = 'http://localhost:8000'
SITETITLE = 'Debian Nantes'
SITESUBTITLE = 'Toute l\'activité sur Debian à Nantes'
SITEDESCRIPTION = 'Debian à Nantes'
SITELOGO = '/images/logo/logo.png'
FAVICON = '/images/favicon.ico'

CC_LICENSE = {
    'name': 'Creative Commons Attribution-ShareAlike',
    'version': '4.0',
    'slug': 'by-sa'
}

COPYRIGHT_YEAR = 2019

MAIN_MENU = True
MENUITEMS = (('Archives', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Tags', '/tags.html'),)

PATH = 'content'

TIMEZONE = 'Europe/Paris'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = ()

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
THEME = 'themes/Flex'

# Enable i18n plugin.
PLUGIN_PATHS = ["plugins"]
PLUGINS = ['i18n_subsites']
# Enable Jinja2 i18n extension used to parse translations.
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n']
}

# Translate to French.
DEFAULT_LANG = 'fr'
OG_LOCALE = 'fr_FR'
LOCALE = 'fr_FR.UTF-8'

# Default theme language.
I18N_TEMPLATES_LANG = 'en'
