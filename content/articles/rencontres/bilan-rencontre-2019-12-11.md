Title: Bilan de la rencontre du 11 décembre 2019
Date: 2020-01-18 17:24

Mercredi 11 décembre, une vingtaine de personnes se sont réunies dans les locaux d’[Epitech](https://www.epitech.eu/fr/ecole-informatique-nantes/)/[E-ArtSup](https://www.e-artsup.net/ecole-graphisme-design-infographie-nantes.aspx) pour la dernière rencontre Debian nantaise de l’année 2019.

Après une rapide présentation du collectif _Debian Nantes_, tvincent a présenté [FreedomBox](https://www.freedombox.org/), une [_Debian Pure Blend_](https://www.debian.org/blends/index.fr.html) dédiée à l’autohébergement. Une démonstration en direct de Freedombox a été réalisée sur une [brique internet](https://internetcu.be/), avec un succès… mitigé. S’en est suivie une petite session de questions/réponses autour du projet, notamment concernant la comparaison de FreedomBox avec [YUNOHOST](https://yunohost.org).

Avant de clore la rencontre, les participants ont échangé sur Debian en général en abordant notamment la [résolution générale sur les systèmes de démarrage et systemd](https://www.debian.org/vote/2019/vote_002), le système de vote ainsi que le rôle du [secrétaire du Projet Debian](https://www.debian.org/devel/secretary.fr.html).

Merci à Epitech/E-ArtSup de nous avoir accueilli et rendez-vous début 2020 pour la prochaine rencontre !