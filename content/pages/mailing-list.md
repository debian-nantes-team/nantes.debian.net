Title: Liste de diffusion
Date: 2019-12-12 13:38

Pour rester informé(e) sur les rencontres Debian Nantes, inscrivez-vous sur la
liste diffusion [Debian Nantes - MeetUp][subscribe].

Les archives sont accessibles ici : [Archives][archives]

[archives]: https://nantes.debian.net/mailman3/hyperkitty/
[subscribe]: https://nantes.debian.net/mailman3/postorius/
