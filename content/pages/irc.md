Title: Salon IRC
Date: 2019-12-12 11:41

Rejoignez-nous sur notre salon IRC pour discuter des événements Debian sur
Nantes.

[#debian-nantes][channel] sur [OFTC][oftc]

[channel]: https://webchat.oftc.net/?channels=#debian-nantes
[oftc]: https://www.oftc.net/

Ce salon est public et ouvert à toutes et tous.

Les archives sont disponibles sur cette page: [archives][archives]

[archives]: /irc/archives
