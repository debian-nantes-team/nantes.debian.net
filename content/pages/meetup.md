Title: Rencontres
Date: 2019-12-15 14:47

Les membres de Debian Nantes organisent périodiquement des rencontres afin de
pouvoir échanger sur des sujets autour du libre, de linux, de Debian ou de ses
dérivés.

Ces rencontres sont gratuites, libres et ouvertes à tous et à toutes.

Nous maintenons une liste de présentations potentielles sur [cette
page][presentations]. À chaque rencontre, les membres présents pourront choisir
un de leurs sujets. Cette liste est prévue pour servir de mémo, ce n'est pas un
programme pour la prochaine rencontre.

Vous pouvez consulter tous les articles sur les rencontres Debian Nantes sur ce
lien : [Liste des rencontres][rencontres]

Vous pouvez également rejoindre notre groupe sur la communauté
GetTogether [Rencontres Debian Nantes][gettogether] ou suivre notre
compte twitter [@debian\_nantes][twitter].

[presentations]: https://nantes.debian.net/pad/MnFAq5BJR-66dqrD_6qDsg
[rencontres]: {category}rencontres
[gettogether]: https://gettogether.community/recontres-debian-nantes/
[twitter]: https://twitter.com/debian_nantes
