# Debian Nantes website

Source code for https://nantes.debian.net.

The source is available on
https://salsa.debian.org/debian-nantes-team/nantes.debian.net.

Production repository is on ssh://git@nantes.debian.net/www.git (restricted
access).

## Workflow

`master` branch is the code deployed on the website. This branch does not accept
push, only merges.

Developments can be done on branch `develop`. When ready it can be merged to
`master`. The website is automatically updated on merge.

For new pages and article a peer review is required.

## Content

Content is organized following this convention:

- `content/pages/`: for main pages, available from the sidebar
- `content/articles/<category>/`: for articles
- `content/images/`: for static images
- `content/files/`: for other static content

Current categories for articles includes:

- `rencontres`: for meetup organisation

Feel free to add on any category that you might need.

## Dependencies

In order to generate and server this website you will need [pelican][pelican].

Install it with:

```bash
sudo apt install -y pelican
```

## Testing

To test the website, just run the development server:

```bash
make devserver
```

You can then access the website at http://localhost:8000.

## Documentation

Links to useful documentation:

- [Markdown cheatsheet][markdown]
- [How to write content with pelican][content]

[markdown]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
[content]: https://docs.getpelican.com/en/stable/content.html
[pelican]: https://blog.getpelican.com/
